#include "Helper.h"
#include <windows.h>
#include <string>

#define PATH_MAX 2048
#define SECRET "Secret.dll"

using namespace std;

typedef int(*secretFunction)();

bool endsWith(const char* text, const char* seg);
DWORD listDirectories(LPSTR dir);

int main(void)
{
	LPSTR curDir = new CHAR[2048];
	string userCommand;
	vector<string> words;
	DWORD dwError = 0;
	// Continues until exists
	while (1)
	{
		// Resetting thread's errors
		SetLastError(0);
		// Getting command from user
		GetCurrentDirectory(2047, curDir); // Leaves room for another /
		curDir[strlen(curDir) + 1] = 0;
		
		// Getting command and removing whitespaces
		wcout << curDir << " >> ";
		getline(cin, userCommand);
		Helper::trim(userCommand);

		// Splitting the command to words
		words = Helper::get_words(userCommand);

		// Making an action according to the command
		if (userCommand == "cwd")
		{
			cout << curDir << endl;
		}
		else if (words[0] == "cd")
		{
			// Checking if cd got exactly 1 parameter
			if (words.size() >= 2)
			{
				SetCurrentDirectory(userCommand.c_str() + 3);
				dwError = GetLastError();
				if (dwError)
				{
					cout << "ERROR : " << dwError << endl;
				}
			}
			else
			{
				cout << "cd receives 1 parameter." << endl;
			}
		}
		else if (words[0] == "create")
		{
			// Checking if create got exactly 1 parameter
			if (words.size() == 2)
			{
				CreateFile((LPCSTR)words[1].c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				dwError = GetLastError();
				if (dwError)
				{
					cout << "ERROR : " << dwError << endl;
				}
			}
			else
			{
				cout << "create receives exactly 1 parameter." << endl;
			}
		}
		else if (userCommand == "ls")
		{
			dwError = listDirectories(curDir);
			if (dwError)
			{
				cout << "ERROR : " << dwError << endl;
			}
		}
		else if (userCommand == "secret")
		{
			HMODULE handle;
			handle = LoadLibrary(SECRET);
			if (handle != NULL)
			{
				secretFunction func = (secretFunction)GetProcAddress(handle, "TheAnswerToLifeTheUniverseAndEverything");
				if (!func)
				{
					cout << "Function Not Found." << endl;
				}
				else
				{
					cout << "Function returned: " << func() << endl;
				}
			}
			else
			{
				dwError = GetLastError();
				cout << "ERROR : " << dwError << endl;
			}
		}
		else if (words.size() == 1 && endsWith(userCommand.c_str(), ".exe"))
		{
			// Variables
			STARTUPINFO startupInfo;
			PROCESS_INFORMATION procInfo;
			LPSTR dir = (char*)userCommand.c_str();
			DWORD exitCode;
			
			// Resetting structs' memory
			ZeroMemory(&startupInfo, sizeof(startupInfo));
			startupInfo.cb = sizeof(startupInfo);
			ZeroMemory(&procInfo, sizeof(procInfo));
			// Trying to run the process
			if (CreateProcess(NULL, dir, NULL, NULL, FALSE, 0, NULL, NULL, &startupInfo, &procInfo))
			{
				WaitForSingleObject(procInfo.hProcess, INFINITE); // waits until the object has signaled to be finished
				if (GetExitCodeProcess(procInfo.hProcess, &exitCode))
				{
					cout << "Exited with code " << exitCode << endl;
				}
				else
				{
					dwError = GetLastError();
					cout << "ERROR : " << dwError << endl;
				}
			}
			else
			{
				dwError = GetLastError();
				cout << "ERROR : " << dwError << endl;
			}
		}
		else if (userCommand == "cls")
		{
			system("cls");
		}
		else if (userCommand == "exit")
		{
			break;
		}
		else
		{
			cout << "Not a valid command." << endl;
		}
		
	}
}

DWORD listDirectories(LPSTR dir)
{
	HANDLE handle;
	WIN32_FIND_DATA wFindData;
	DWORD errorCode = 0;
	LPSTR str = new CHAR[2048];

	// Getting "first file" full directory
	strncpy(str, dir, 2048);
	strcat(str, "\\*.*");

	// Getting first file
	handle = FindFirstFileA(str, &wFindData);
	// Checking for errors
	errorCode = GetLastError();
	if (!errorCode)
	{
		// printing all files in directory
		do
		{
			cout << wFindData.cFileName << endl;
		} while (FindNextFile(handle, &wFindData));
	}
	return errorCode;
}

///<summary>returns whether or not the text ends with the segment</summary>
bool endsWith(const char* text, const char* seg)
{
	int len1 = strlen(text), len2 = strlen(seg);
	bool doesEnd = false;
	if (len1 > len2)
	{
		doesEnd = true;
		for (int i = len1 - len2, j = 0; i < len1; i++, j++)
		{
			if (text[i] != seg[j])
			{
				doesEnd = false;
				break;
			}
		}
	}
	return doesEnd;
}